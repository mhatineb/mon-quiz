
/** Variable des réponses de chaque question */
   let answers = document.querySelectorAll<HTMLButtonElement>('.answer');

/** Variable pour défiler le n° de la question  */
   let label:any = document.querySelector<HTMLElement>('#label');

   const labelDiv= document.querySelector<HTMLDivElement>('.label');

/** Variable pour l'activation du gif quand on click sur la bonne réponse */       
    const goodResult = document.querySelector<HTMLElement>('.gifGood');
 
/** Variable pour l'activation du gif quand on click sur la mauvaise réponse */
    const falseResult = document.querySelector<HTMLElement>('.gifFalse');
      
/** Variable du bouton continuer pour changer de question */
    const nextQuest= document.querySelector<HTMLButtonElement>('.continue'); 

    const divContinue =document.querySelector<HTMLDivElement>('.buttonContinue');

/** Variable de la div question pour qu'elle soit remplacé par le gif suite au click */    
    const block= document.querySelector<HTMLElement>('.block');

/** Variable pour que ça passe à la question suivante */    
    const loadP = document.querySelector('#quest');  

/** Variable pour la page résultat */
    const showResult = document.querySelector<HTMLElement>('.showResult');

    const img =document.querySelector<HTMLImageElement>('.tim');

/** Variable pour le bouton score */
let score = document.querySelector('#score');
let counterScore = 0;
if (score) {
   score.innerHTML = String(counterScore);
}
    
/** Variable pour augmenter les N° de questions */
let addQuestion = 0;

/** Variable pour passer les questions/réponses */
let questionNow = 0;

/** Booléen qui permet de changer de questions/réponses */
let isCorrect = false;


/** 
 * Boucle pour les boutons de réponses
 * */
for (const answer of answers) {
   answer.addEventListener('click', () => {
      
      for (const answerA of answers) {
        
        answerA.classList.remove('isActive'); 
        answer.classList.add('isActive');
      }
      
       
     

      if (answer.textContent == questions[questionNow].goodOne)
      {    
         isCorrect = true;
      }
        else 
      {
         isCorrect = false;
      }
      if (answer.onclick)
         answer.disabled= true;
   }) 
}



/**  
 * Conditions du Bouton "continuer" 
 * */
nextQuest?.addEventListener('click', () => {
   
   if (questionNow<questions.length) {
       questionNow++;
       
       for (const answerA of answers) {
        
         answerA.classList.remove('isActive'); 

       }
       if (questionNow > 4) {
         finish()
      }
      } 

      loadQuestion(questions[questionNow]);
      if (isCorrect) {
         addScore(score);
         showGoodResult();

      } else if (!isCorrect) {
         showFalseResult();
      } 
    }
    
)   

/**
 * Cache les reponses pour afficher un gif 'goodOne'
 */
function showGoodResult() {
   if (goodResult) {
      goodResult.style.display = "block";
   }
   if (block) {
      block.style.display = 'none';
   }
   if (nextQuest) {
      nextQuest.disabled = true;
   }

   setTimeout(()=>{
   if (goodResult) {
      goodResult.style.display = "none";
   }
   if (block) {
      block.style.display = 'grid';
   }
   if (nextQuest) {
      nextQuest.disabled = false;
   }
   
   }, 1500)
}  

/**
 * Cache les reponses pour afficher un gif 'badOne'
 */
function showFalseResult() {
   if (falseResult) {
      falseResult.style.display = "block";
   }
         if (block) {
            block.style.display = 'none';
         }
         if (nextQuest) {
            nextQuest.disabled = true;
         }
         setTimeout(()=>{
            if (falseResult) {
               falseResult.style.display = "none";
            }
            if (block) {
               block.style.display = 'grid';
            }
            if (nextQuest) {
               nextQuest.disabled = false;
            }
            }, 1500)
}

/** 
 * Fonction qui permet d'augmenter le score si on click sur la bonne réponse
 * */    
function addScore(scr: Element | null) {
   if (scr) {
      counterScore += 10;
      scr.innerHTML = String(counterScore);
   }
}   

/** 
 * Fonction pour que ça passe à la question suivante
 * */
function loadQuestion(question: any) {
   
   let i = 0;
   if (addQuestion <= 4) {
      addQuestion++;
      
   } 
    
   for (const answer of answers) {
      answer.innerHTML = String (question.response[i]);
      i++;
      // answer.style.backgroundColor = "black";
      // answer.style.color = "white";
   }
   
   /** 
    * Condition pour que ça passe à la question suivante
    * */      
   if (loadP) {
      loadP.innerHTML = question.question;
   }
   if (nextQuest) {
      label.innerHTML = 'Question ' + addQuestion;  
   }  
   
}    

/**
 * Objet de toutes les questions
 * */
let questions = [
   {  
      question: 'Dans quel film de Tim Burton, le héros a-t-il une paire de ciseaux à la place de ses mains ?',
      goodOne: 'A. Edouard aux mains d\'argent',
      response: [
         'A. Edouard aux mains d\'argent',
         'B. L\'étrange noël de monsieur Jack',
         'C. Beetlejuice',
         'D. Les noces funèbres'
      ]
   },

   {  
      question: 'Dans quel film de Tim Burton, un cavalier sans tête sème-t-il la terreur dans un village ?',
      goodOne: 'C. Sleepy Hollow',
      response: [
         'A. Sweeney Todd',
         'B. Ed Wood',
         'C. Sleepy Hollow',
         'D. Les noces funèbres'
      ]
      
   },

   {  
      question: 'Quel film réalisé par Tim Burton en 2001 est un remake d’un succès des années 70 ?',
      goodOne: 'D. La planète des singes',
      response: [
         'A. Big Fish',
         'B. Charlie et la chocolaterie',
         'C. Big Eyes',
         'D. La planète des singes'
      ]
   },

   {  
      question: 'Quel acteur britannique tient le premier rôle dans le film Big Fish en 2003 ?',
      goodOne: 'B. Evan McGregor',
      response: [
         'A. Hugh Grant',
         'B. Evan McGregor',
         'C. Alan Rickman',
         'D. Kenneth Branagh'
      ]
   },
   {
      question: 'Quel super héros Tim Burton va-t-il mettre image au cinéma en 1989 ?',
      goodOne: 'B. Batman',
      response: [
         'A. Iron Man',
         'B. Batman',
         'C. Superman',
         'D. Wonder Woman'
      ]
   }
] 

/** 
 * Appelle la function loadQuestion et fait apparaitre les réponses et les questions
 * */
loadQuestion(questions[0]);

function finish() {
   
   
      if(showResult){
      showResult.style.display = 'grid';
   }
      if(img){
         img.classList.remove('hide');
      }

      if(block){
      block.style.display = 'none';
   }

   if (divContinue){
      divContinue.style.display ='none';
   }
   if(labelDiv){
      labelDiv.style.display = 'none';
   }
   
      
   }
   
   





   








