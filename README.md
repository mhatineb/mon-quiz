# Mon quiz

J'ai commencé par faire un wireframe/maquette avec Figma
j'ai créé mon dossier vscode avec l'installation de parcels et du package.json puis j'ai mis dans le fichier .gitignore: - node_modules
                                - dist
                                - .parcel-cache
pour éviter de tout copier sur mon gitlab.
J'ai créé le dossier public pour le HTML, le CSS et les images
J'ai créé le dossier src pour le fichier TS

Ce quiz contient:
- une image
- un h1 
- une div pour le score
- une div 'container' regroupant la vignette qui contient:
 l'affichage du N° de la question, la question et les propositions de reponses actuelles.Puis le bouton "continuer" afin de remplacer la div question/responses par le gif "goodOne" ou "badOne" ainsi qu'implimenter le score si la réponse est bonne.
 J'ai un bug à la dernière question que je n'ai pas pu résoudre à temps et qui ne permet pas de valider le score de la dernière ni de passer à la div 'showResult'
 J'espère pouvoir l'améliorer plus tard.

Le site est responsive
